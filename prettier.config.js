module.exports = {
    // LAST UPDATE v1.18.1
    printWidth: 150,
    tabWidth: 4,
    singleQuote: true,
    quoteProps: 'consistent',
    arrowParens: 'always',
    endOfLine: 'lf',
};
