import { bindable, customElement } from 'aurelia-framework';
import { IIOAdapter } from 'game/io-adapter.interface';
import { IOutputMessage } from 'game/output-message.interface';

@customElement('terminal')
export class Terminal {
    private history: string[] = [];
    private historyPosition: number = -1;

    protected data: IOutputMessage[] = [];
    protected command: string = '';

    @bindable public ioAdapter!: IIOAdapter;

    protected process(): void {
        console.log('process');
    }

    protected processKeyPress(event: KeyboardEvent): void {
        switch (event.keyCode) {
            case 67:
                if (event.ctrlKey) {
                    this.command = '';
                }
                break;
            case 40:
                // arrow down
                if (this.historyPosition >= this.history.length) {
                    this.historyPosition = -1;
                }
                this.command = this.history[++this.historyPosition];
                break;
            case 38:
                // arrow up
                this.historyPosition = this.historyPosition === -1 ? this.history.length : this.historyPosition;
                this.command = this.history[--this.historyPosition];
                break;
            case 13:
                if (this.command !== '') {
                    this.historyPosition = -1;
                    this.history.push(this.command);
                    this.data.push({ text: this.command, css: 'default', isCommand: true });
                    this.ioAdapter.input(this.command);
                    this.command = '';
                }
                break;
            default:
                console.info('Unused key', event);
                break;
        }

        const terminal: HTMLElement | null = document.getElementById('terminal-display');
        if (terminal) {
            setTimeout(() => {
                terminal.scrollTop = terminal.scrollHeight;
            }, 200);
        }
    }

    public bind(): void {
        this.ioAdapter.onOutput = (out: IOutputMessage) => {
            this.data.push(out);
        };
        this.ioAdapter.input('start');
    }
}
