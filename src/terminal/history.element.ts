import { bindable, customElement } from 'aurelia-framework';
import { IOutputMessage } from 'game/output-message.interface';

@customElement("history")
export class History {
  @bindable public data: IOutputMessage[] = [];
}
