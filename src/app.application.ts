import { Game } from 'game/game.model';
import { IIOAdapter } from 'game/io-adapter.interface';
import { IOAdapter } from 'io-adapter.service';

export class App {
    public ioAdapter: IIOAdapter = new IOAdapter();
    protected game: Game = new Game(this.ioAdapter);
    protected help: string = App.keys.join(', ');
    private static keys: string[] = [
        '<em>enter</em> - to submit command',
        '<em>arrows</em> - to browse history',
        '<em>ctrl + c</em> - to cancel current command',
    ];
}
