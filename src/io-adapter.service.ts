import { IIOAdapter } from 'game/io-adapter.interface';
import { IOutputMessage } from 'game/output-message.interface';

export class IOAdapter implements IIOAdapter {
    public output(out: IOutputMessage): void {
        this.onOutput(out);
    }
    public onOutput(out: IOutputMessage): void {
        console.log(out);
    }
    public input(input: string): void {
        this.onInput(input);
    }
    public onInput(input: string): void {
        console.log(input);
    }
}
