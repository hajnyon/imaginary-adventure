import { IOutputMessage } from './output-message.interface';

export interface IIOAdapter {
  output(out: IOutputMessage): void;
  onOutput(out: IOutputMessage): void;
  input(input: string): void;
  onInput(input: string): void;
}
