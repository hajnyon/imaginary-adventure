import { IAction } from './action.interface';
import { IInputMessage } from './input-message.interface';
import { IOutputMessage } from './output-message.interface';

export type TAction = { [key: string]: IAction };

export abstract class AActionable {
    protected availableActions: TAction;

    public doAction(input: IInputMessage): IOutputMessage | null {
        if (this.availableActions[input.command]) {
            const action: IAction = this.availableActions[input.command];
            return action.callback(input);
        }
        return null;
    }

    public getHelpMessage(namespace: string): IOutputMessage | null {
        let commands: string = '';
        let first: boolean = true;
        for (const command in this.availableActions) {
            if (this.availableActions.hasOwnProperty(command)) {
                commands += `${first ? '' : ', '}${command}`;
                first = false;
            }
        }
        const text: string = `<b>${namespace} commands</b>: <em>${commands}</em>`;
        return { text: text, css: 'help' };
    }

    public getCommandHelpMessage(command: string): IOutputMessage | null {
        if (!this.availableActions[command]) {
            return null;
        }
        const text: string = `<b>${command}</b>:<br>usage: <em>${this.availableActions[command].usage}</em><br>help: <em>${this.availableActions[command].help}</em>`;
        return { text: text, css: 'help' };
    }
}
