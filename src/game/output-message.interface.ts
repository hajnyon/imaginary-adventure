export interface IOutputMessage {
    text: string;
    isCommand?: boolean;
    css: string;
}
