import { AActionable, TAction } from './actionable.abstract';
import { GameFactory } from './game-factory.model';
import { IInputMessage } from './input-message.interface';
import { IIOAdapter } from './io-adapter.interface';
import { IOutputMessage } from './output-message.interface';
import { Player } from './player/player.model';
import { AItem } from './world/items/item.abstract';
import { World } from './world/world.model';

export class Game extends AActionable {
    private world: World;
    private player: Player;

    protected availableActions: TAction = {
        start: {
            usage: 'start',
            help: 'Shows the opening text.',
            supportsValue: false,
            callback: (_input: IInputMessage) => {
                return {
                    text: `Greetings, traveler! You are brave to come to this hostile land full of dangers. There are few monsters that lives in this area and the people living here would very much appreciate you clearing all of the bloodsucking creatures. I wish you luck.<br>If you are lost try typing 'help' or 'help [COMMAND]'.`,
                    css: '',
                };
            },
        },
        restart: {
            usage: 'restart',
            help: 'Resets the game.',
            supportsValue: false,
            callback: (_input: IInputMessage) => {
                this.init();
                const message: IOutputMessage = this.doAction({ command: 'start' });
                return {
                    text: `Game restarted.<br>${message.text}`,
                    css: '',
                };
            },
        },
        help: {
            usage: 'help | help [COMMAND]',
            help: 'Shows available commands or description of specific one.',
            supportsValue: true,
            callback: (input: IInputMessage) => {
                if (input.value) {
                    return this.getCommandHelpMessage(input.value);
                }
                return this.getHelpMessage('Game');
            },
        },
        attack: {
            usage: 'attack [WHO] [WITH_WHAT]',
            help: 'Attacks enemy with weapon.',
            supportsValue: true,
            callback: (input: IInputMessage) => this.handleAttack(input),
        },
        pickup: {
            usage: 'pickup [WHAT]',
            help: 'Picks up item.',
            supportsValue: true,
            callback: (input: IInputMessage) => this.handlePickUp(input),
        },
    };

    public constructor(private ioAdapter: IIOAdapter) {
        super();
        this.ioAdapter.onInput = (input: string) => {
            this.handleInput(input);
        };
        this.init();
    }

    private init(): void {
        this.world = GameFactory.build();
        console.log('Game -> init -> this.world', this.world);
        this.player = new Player('player');
    }

    private parseInput(text: string): IInputMessage {
        const split: string[] = text.split(' ');
        if (split.length < 1) {
            throw new Error('No command.');
        } else if (split.length === 1) {
            return { command: split[0] };
        } else if (split.length === 2) {
            return { command: split[0], value: split[1] !== '' ? split[1] : undefined };
        }
        return { command: split[0], value: split[1] !== '' ? split[1] : undefined, valueSecond: split[2] !== '' ? split[2] : undefined };
    }

    private handleInput(input: string): void {
        try {
            const inputMessage: IInputMessage = this.parseInput(input);
            let outputMessages: (IOutputMessage | null)[] = [];

            outputMessages.push(this.doAction(inputMessage));
            outputMessages.push(this.player.doAction(inputMessage));
            outputMessages.push(this.world.doAction(inputMessage));
            outputMessages = outputMessages.filter((item: IOutputMessage | null) => item !== null);
            if (outputMessages.length < 1) {
                throw new Error(`Command '${inputMessage.command}' has no action bound to it.`);
            }
            for (const message of outputMessages) {
                this.ioAdapter.output(message);
            }
        } catch (error) {
            this.ioAdapter.output({ text: error.message, css: 'error' });
        }
    }

    private handleAttack(input: IInputMessage): IOutputMessage | null {
        try {
            return this.world.attack(input, this.player);
        } catch (error) {
            throw error;
        }
    }

    private handlePickUp(input: IInputMessage): IOutputMessage | null {
        try {
            return this.world.pickUp(input, this.player);
        } catch (error) {
            throw error;
        }
    }
}
