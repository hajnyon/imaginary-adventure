import { AActionable, TAction } from 'game/actionable.abstract';
import { IInputMessage } from 'game/input-message.interface';
import { AItem } from 'game/world/items/item.abstract';
import { PotionItem } from 'game/world/items/potion.model';
import { WeaponItem } from 'game/world/items/weapon.model';

export class Player extends AActionable {
    private bag: AItem[] = [new WeaponItem('dagger', 2)];
    private hp: number = 20;
    private alive: boolean = true;

    protected availableActions: TAction = {
        name: {
            usage: 'name [NAME]',
            help: 'Sets your name.',
            supportsValue: true,
            callback: (input: IInputMessage) => {
                if (!input.value || input.value === '') {
                    throw new Error(`Set name to what?`);
                }
                this.name = input.value;
                return { text: `Your name set to ${input.value}`, css: '' };
            },
        },
        who: {
            usage: 'who',
            help: 'Shows your name.',
            supportsValue: false,
            callback: (_input: IInputMessage) => {
                return { text: this.name, css: 'name' };
            },
        },
        bag: {
            usage: 'bag',
            help: 'Shows items in your bag.',
            supportsValue: false,
            callback: (_input: IInputMessage) => {
                const items: string[] = [];
                for (const item of this.bag) {
                    items.push(item.name);
                }
                return { text: items.length > 0 ? items.join(', ') : 'Bag is empty.', css: 'bag' };
            },
        },
        use: {
            usage: 'use [ITEM]',
            help: 'Use item from bag.',
            supportsValue: true,
            callback: (input: IInputMessage) => {
                if (!input.value) {
                    throw new Error('User what?');
                }
                const itemIndex: number = this.bag.findIndex((item: AItem) => item.name === input.value);
                if (itemIndex < 0) {
                    throw new Error(`No item '${input.value}' in bag.`);
                }
                const item: AItem = this.bag[itemIndex];
                if (item instanceof PotionItem) {
                    this.heal(item);
                    return { text: `Healed for ${item.heal}. HP: ${this.hp}`, css: 'healed' };
                }
                this.bag.splice(itemIndex, 1);
                return { text: `Can't use '${item.name}'.`, css: '' };
            },
        },
        help: {
            usage: 'help | help [COMMAND]',
            help: 'Shows available commands or description of specific one.',
            supportsValue: true,
            callback: (input: IInputMessage) => {
                if (input.value) {
                    return this.getCommandHelpMessage(input.value);
                }
                return this.getHelpMessage('Player');
            },
        },
    };

    public constructor(public name: string) {
        super();
    }

    private heal(potion: PotionItem): void {
        this.hp += potion.heal;
    }

    public canAttack(input: IInputMessage): AItem {
        if (!input.value) {
            throw new Error(`Attack what?`);
        }
        if (!input.valueSecond) {
            throw new Error(`Attack '${input.value}' with what?`);
        }

        const weaponExist: AItem | undefined = this.bag.find((item: AItem) => item.name === input.valueSecond);
        if (!weaponExist) {
            throw new Error(`Weapon '${input.valueSecond}' is not in your bag.`);
        }

        return weaponExist;
    }

    public receiveDamage(damage: number): boolean {
        this.hp -= damage;
        if (this.hp <= 0) {
            this.alive = false;
        }
        return this.alive;
    }

    public addToBag(item: AItem): void {
        this.bag.push(item);
    }

    public getHP(): number {
        return this.hp;
    }
}
