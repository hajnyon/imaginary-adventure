import { IInputMessage } from './input-message.interface';
import { IOutputMessage } from './output-message.interface';

export interface IAction {
    usage: string;
    help: string;
    supportsValue: boolean;
    callback(input: IInputMessage): IOutputMessage | null;
}
