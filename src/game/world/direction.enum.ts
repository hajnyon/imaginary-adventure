export enum EDirection {
    NORTH = 'north',
    EAST = 'east',
    WEST = 'west',
    SOUTH = 'south',
}
