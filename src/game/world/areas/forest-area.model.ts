import { AArea } from './area.abstract';

export class ForestArea extends AArea {
    public description: string = `You came to forest. It's dense that you can't almost see the sky, air is humid and you hear noises from everywhere.`;
}
