import { AArea } from './area.abstract';

export class DesertArea extends AArea {
    public description: string = `Sand everywhere you look. Nothing except sand and occasional rock. Nothing ... or is this a foot print?!`;
}
