import { IInputMessage } from 'game/input-message.interface';
import { IOutputMessage } from 'game/output-message.interface';

import { EDirection } from '../direction.enum';
import { IAttackOutput } from '../enemies/attack-output.interface';
import { Enemy } from '../enemies/enemy.model';
import { AItem } from '../items/item.abstract';

export abstract class AArea {
    protected description: string;
    protected cameFrom: EDirection | null = null;

    public constructor(protected enemies: Enemy[], protected items: AItem[]) {}

    public isVisited(): boolean {
        return this.cameFrom !== null;
    }

    public isClear(): boolean {
        return this.enemies.length <= 0;
    }

    public setCameFrom(direction: EDirection): void {
        switch (direction) {
            case EDirection.NORTH:
                this.cameFrom = EDirection.SOUTH;
                break;
            case EDirection.SOUTH:
                this.cameFrom = EDirection.NORTH;
                break;
            case EDirection.WEST:
                this.cameFrom = EDirection.EAST;
                break;
            case EDirection.EAST:
                this.cameFrom = EDirection.WEST;
                break;
            default:
                break;
        }
    }

    public explore(): IOutputMessage {
        let text: string = `${this.description}<br>`;
        if (this.enemies.length > 0) {
            const count: string = this.enemies.length > 1 ? `are ${this.enemies.length} enemies` : `is an enemy`;
            text += `Keep your eyes open! There ${count} in front of you! `;
            for (const enemy of this.enemies) {
                text += `<i>${enemy.name}</i> `;
            }
            text += '<br>';
        }
        if (this.items.length > 0) {
            const count: string = this.items.length > 1 ? `are ${this.items.length} items` : `is an item`;
            text += `Ah look! There ${count} laying on ground! `;
            for (const item of this.items) {
                text += `<i>${item.name}</i> `;
            }
        }
        return { text: text, css: '' };
    }

    public canMove(direction: EDirection): boolean {
        if (direction === this.cameFrom) {
            return true;
        }
        for (const enemy of this.enemies) {
            if (enemy.isAlive()) {
                throw new Error(`Enemy '${enemy.name}' is still alive and is blocking your way!`);
            }
        }

        return true;
    }

    public canPickUp(): boolean {
        return this.items.length > 0;
    }

    public pickUp(input: IInputMessage): AItem {
        const itemIndex: number = this.items.findIndex((item: AItem) => item.name === input.value);
        if (itemIndex < 0) {
            throw new Error(`There is no '${input.value}' around.`);
        }
        return this.items.splice(itemIndex, 1)[0];
    }

    public attack(input: IInputMessage, weapon: AItem): IAttackOutput {
        const enemyIndex: number = this.enemies.findIndex((enemy: Enemy) => enemy.name === input.value);
        if (enemyIndex < 0) {
            throw new Error(`There is no '${input.value}' around.`);
        }
        const attackOut: IAttackOutput = this.enemies[enemyIndex].attack(weapon);
        if (!attackOut.stillAlive) {
            this.enemies.splice(enemyIndex, 1);
        }
        return attackOut;
    }
}
