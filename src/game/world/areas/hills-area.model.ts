import { AArea } from './area.abstract';

export class HillsArea extends AArea {
    public description: string = 'You came to hills area. Wind is howling, grass is glittering and you feel light breeze on your face.';
}
