import { AActionable, TAction } from 'game/actionable.abstract';
import { IInputMessage } from 'game/input-message.interface';
import { IOutputMessage } from 'game/output-message.interface';
import { Player } from 'game/player/player.model';

import { AArea } from './areas/area.abstract';
import { ICoords } from './coords.interface';
import { EDirection } from './direction.enum';
import { IAttackOutput } from './enemies/attack-output.interface';
import { AItem } from './items/item.abstract';

export class World extends AActionable {
    private current: {
        area: AArea;
        coords: ICoords;
    };

    protected availableActions: TAction = {
        go: {
            usage: `go [${World.directions.join('|')}]`,
            help: 'Moves the player across the map.',
            supportsValue: true,
            callback: (input: IInputMessage) => {
                return this.handleMove(input);
            },
        },
        explore: {
            usage: 'explore',
            help: 'Look where you are and what is around you.',
            supportsValue: false,
            callback: (_input: IInputMessage) => this.current.area.explore(),
        },
        map: {
            usage: 'map',
            help: 'Show map.',
            supportsValue: false,
            callback: (_input: IInputMessage) => this.displayMap(),
        },
    };

    private static directions: EDirection[] = [EDirection.EAST, EDirection.WEST, EDirection.NORTH, EDirection.SOUTH];

    public constructor(private areas: AArea[][], currentArea: { area: AArea; coords: ICoords }) {
        super();
        this.current = currentArea;
    }

    private displayMap(): IOutputMessage {
        let message: string = ``;
        for (const areas of this.areas) {
            for (const area of areas) {
                const isClear: boolean = area.isClear();
                const isVisited: boolean = area.isVisited();
                const isCurrent: boolean = area === this.current.area;
                if (isVisited && isClear) {
                    message += `| ${isCurrent ? '<span class="current-position">' : ''}x${isCurrent ? '</span>' : ''} `;
                } else if (isVisited) {
                    message += `| ${isCurrent ? '<span class="current-position">' : ''}o${isCurrent ? '</span>' : ''} `;
                } else {
                    message += '| _';
                }
            }
            message += ` |<br>`;
        }
        message += `x - visited and no monsters, o - visited and monsters remaining, _ - not visited, <span class="current-position">blue</span> - current position`;
        return { text: message, css: '' };
    }

    private handleMove(input: IInputMessage): IOutputMessage | null {
        try {
            if (!input.value || !World.directions.includes(<EDirection>input.value.toLowerCase())) {
                throw new Error(`Can't go to direction '${input.value}'!`);
            }
            const direction: EDirection = <EDirection>input.value;

            this.current.area.canMove(direction);

            const newCoords: ICoords = this.checkDirectionMove(direction);
            this.current = {
                area: this.areas[newCoords.y][newCoords.x],
                coords: newCoords,
            };
            this.current.area.setCameFrom(direction);

            return { text: `You went ${direction}<br>${this.current.area.explore().text}`, css: '' };
        } catch (error) {
            throw error;
        }
    }

    private checkDirectionMove(direction: EDirection): ICoords {
        const currentCoords: ICoords = this.current.coords;
        const newCoords: ICoords = {
            x: currentCoords.x,
            y: currentCoords.y,
        };
        switch (direction) {
            case EDirection.NORTH:
                this.isValidCoords({ x: newCoords.x, y: newCoords.y - 1 });
                newCoords.y--;
                break;
            case EDirection.SOUTH:
                this.isValidCoords({ x: newCoords.x, y: newCoords.y + 1 });
                newCoords.y++;
                break;
            case EDirection.WEST:
                this.isValidCoords({ x: newCoords.x - 1, y: newCoords.y });
                newCoords.x--;
                break;
            case EDirection.EAST:
                this.isValidCoords({ x: newCoords.x + 1, y: newCoords.y });
                newCoords.x++;
                break;
            default:
                throw new Error(`Cannot go to direction '${direction}'.`);
        }
        return newCoords;
    }

    private isValidCoords(coords: ICoords): void {
        if (coords.y > this.areas.length - 1 || coords.y < 0) {
            throw new Error('Cannot go this way, mountains block you.');
        }
        if (coords.x > this.areas[0].length - 1 || coords.x < 0) {
            throw new Error('Cannot go this way, mountains block you.');
        }
    }

    private checkWinningCondition(): string {
        for (const areas of this.areas) {
            for (const area of areas) {
                if (!area.isClear()) {
                    return '';
                }
            }
        }
        return `<br><span class="winner">Congratulations! You cleared whole land from all the monsters!</span>`;
    }

    public pickUp(input: IInputMessage, player: Player): IOutputMessage | null {
        try {
            if (!input.value) {
                throw new Error(`Pick up what?`);
            }

            this.current.area.canPickUp();
            const item: AItem = this.current.area.pickUp(input);
            player.addToBag(item);

            return { text: `You picked up ${item.name}.`, css: '' };
        } catch (error) {
            throw error;
        }
    }

    public attack(input: IInputMessage, player: Player): IOutputMessage {
        const enemyName: string = input.value;
        const weapon: AItem = player.canAttack(input);
        const attackOut: IAttackOutput = this.current.area.attack(input, weapon);
        const playerAlive: boolean = player.receiveDamage(attackOut.damageGiven);
        let message: string;
        if (!playerAlive) {
            message = `</span> Enemy '${enemyName}' hit you back for ${attackOut.damageGiven} and you died!<br><span class="game-over">Game over!</span>`;
        } else {
            if (attackOut.stillAlive) {
                message = `.</span> <span class="received">It is still alive and hit you back for ${attackOut.damageGiven}.</span>`;
                message += `<br>Your HP: ${player.getHP()}, '${enemyName}'s' HP: ${attackOut.hp}`;
            } else {
                message = ` and you killed it!</span>`;
                message += `<br>Your HP: ${player.getHP()}`;
                message += this.checkWinningCondition();
            }
        }
        return { text: `<span class="dealt">You dealt ${attackOut.damageReceived} damage to '${enemyName}'${message}`, css: 'attack' };
    }
}
