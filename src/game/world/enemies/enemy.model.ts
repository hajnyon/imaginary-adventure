import { AItem } from '../items/item.abstract';
import { IAttackOutput } from './attack-output.interface';

export class Enemy {
    private alive: boolean = true;
    private hp: number = 10;
    private attackPower: number = 0;
    public readonly name: string;

    public constructor(name: string, attack: number, hp: number) {
        this.name = name;
        this.attackPower = attack;
        this.hp = hp;
    }

    public isAlive(): boolean {
        return this.alive;
    }

    public attack(weapon: AItem): IAttackOutput {
        const damage: number = weapon.attack;
        this.hp -= damage;
        if (this.hp <= 0) {
            this.alive = false;
        }
        return { damageReceived: damage, stillAlive: this.alive, damageGiven: this.alive ? this.attackPower : 0, hp: this.hp };
    }
}
