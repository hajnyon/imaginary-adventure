export interface IAttackOutput {
    damageReceived: number;
    damageGiven: number;
    hp: number;
    stillAlive: boolean;
}
