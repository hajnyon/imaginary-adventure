import { AItem } from './item.abstract';

export class WeaponItem extends AItem {
    public constructor(name: string, attack: number) {
        super();
        this.name = name;
        this.attack = attack;
    }
}
