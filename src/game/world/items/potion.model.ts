import { AItem } from './item.abstract';

export class PotionItem extends AItem {
    public constructor(name: string, attack: number, public heal: number) {
        super();
        this.name = name;
        this.attack = attack;
    }
}
