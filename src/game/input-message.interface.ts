export interface IInputMessage {
    command: string;
    value?: string;
    valueSecond?: string;
}
