import { AArea } from './world/areas/area.abstract';
import { DesertArea } from './world/areas/desert-area.model';
import { ForestArea } from './world/areas/forest-area.model';
import { HillsArea } from './world/areas/hills-area.model';
import { EDirection } from './world/direction.enum';
import { Enemy } from './world/enemies/enemy.model';
import { AItem } from './world/items/item.abstract';
import { PotionItem } from './world/items/potion.model';
import { WeaponItem } from './world/items/weapon.model';
import { World } from './world/world.model';

function getRandomInt(min: number = 0, max: number = 100): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export class GameFactory {
    private static size: number = 5;
    private static potionChance: number = 25;
    private static enemyChance: number = 30;
    private static weaponChance: number = 30;

    private static weaponTypes: string[] = ['sword', 'axe', 'dagger'];
    private static enemiesTypes: string[] = ['striga', 'werewolf', 'golem'];

    public static build(): World {
        const areas: AArea[][] = [];

        let potionsCount: number = 0;
        let enemiesCount: number = 0;
        let weaponsCount: number = 0;

        for (let i = 0; i < GameFactory.size; i++) {
            const subAreas: AArea[] = [];

            for (let j = 0; j < GameFactory.size; j++) {
                const items: AItem[] = [];
                const enemies: Enemy[] = [];

                if (getRandomInt() < GameFactory.potionChance) {
                    items.push(new PotionItem('health-potion', 0, 5));
                    potionsCount++;
                }

                if (getRandomInt() < GameFactory.weaponChance) {
                    const item: AItem = new WeaponItem(
                        GameFactory.weaponTypes[getRandomInt(0, GameFactory.weaponTypes.length - 1)],
                        getRandomInt(3, 10)
                    );
                    items.push(item);
                    weaponsCount++;
                }
                if (getRandomInt() < GameFactory.enemyChance) {
                    const enemy: Enemy = new Enemy(
                        GameFactory.enemiesTypes[getRandomInt(0, GameFactory.enemiesTypes.length - 1)],
                        getRandomInt(3, 10),
                        getRandomInt(5, 12)
                    );
                    enemies.push(enemy);
                    enemiesCount++;
                }

                let area: AArea;
                switch (getRandomInt(0, 2)) {
                    case 0:
                        area = new HillsArea(enemies, items);
                        break;
                    case 1:
                        area = new ForestArea(enemies, items);
                        break;
                    case 2:
                    default:
                        area = new DesertArea(enemies, items);
                        break;
                }
                subAreas.push(area);
            }

            areas.push(subAreas);
        }

        console.log(
            `Generated: ${GameFactory.size * GameFactory.size} areas, ${enemiesCount} enemies, ${weaponsCount} weapons and ${potionsCount} potions.`
        );

        areas[0][0].setCameFrom(EDirection.WEST);
        return new World(areas, { area: areas[0][0], coords: { x: 0, y: 0 } });
    }
}
